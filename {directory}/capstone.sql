-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2016 at 06:48 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `capstone`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `NUID` varchar(10) NOT NULL,
  `Fname` varchar(20) NOT NULL,
  `Lname` varchar(20) NOT NULL,
  `Email` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`NUID`, `Fname`, `Lname`, `Email`) VALUES
('1231', 'jubin', 'vaidya', 'jvaidya'),
('234', 'saumil', 'sharma', 'ssharma@unomaha.edu'),
('47742191', 'Amal', 'Kocherla', 'akocherla'),
('3429384', 'akash', 'sharma', 'akash@unomaha.edu'),
('123123', 'vandan', 'wild', 'jwaild@unomaha.edu'),
('78567345', 'sunny', 'leone', 'sunny@unomaha.edu'),
('79302029', 'jackline', 'fernandez', 'jfernandez@unomaha.e'),
('4564564', 'asdfasd', 'adfsasdf', 'asdfasdf'),
('1231239', 'safsdf', 'asdfas', 'zcxvrq11'),
('123179', 'eeee', 'ddddddd', 'ssssssss'),
('1231231', 'eeeeeeeeeeee', 'qqqqqqqqqqqqq', 'ccccccccccccccc'),
('12312', 'hahah', 'sdsds', 'hhehehe'),
('87879879', 'skdjfoa', 'ansnaan', 'pppppppp'),
('Enter NUID', 'Enter First Name', 'Enter Last Name', 'Enter Email'),
('Enter NUID', 'Enter First Name', 'Enter Last Name', 'Enter Email'),
('981723', 'jay', 'jay', 'jay'),
('123989', 'sdfkjdkj', 'kjdsfjkj', 'fskkjfodi'),
('878789', 'saskjkj', 'skdjfksj', 'skdjfkdjf'),
('432342', 'cap', 'cap', 'jimi@unomaha.edu'),
('9128301928', 'Saumil', 'sksisk', 'aksdjfalkdsjf'),
('12319', 'ajsdkfj', 'ppppppp', 'skjskjd'),
('3333333333', 'aaaaaa', 'bbbb', 'ssssssssss'),
('78567323', 'Eliza ', 'Gautam', 'egautam@gmal.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
