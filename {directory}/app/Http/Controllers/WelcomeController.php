<?php namespace App\Http\Controllers;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
//	public function theme()
//	{
//		return view('Theme');
//	}

	public function app()
	{
		return view('app');
	}
 public function Login()
{
	// return view('secondPage');
	return view('auth.login');
	//return view('Theme');
}
	public function welcome()
	{
		// return view('secondPage');
		return view('welcome');
	}
}
