<html>

		<title>Laravel</title>

				<!DOCTYPE html>
				<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
				<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
				<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
				<!--[if gt IE 8]><!--> <html class="no-js"><!--<![endif]-->

				<head>
					<meta charset="utf-8"/>
					<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
					<!-- InstanceBeginEditable name="doctitle" -->
					<title>University of Nebraska Omaha | <!-- Page Title --></title>
					<meta content="" name="description"/>
					<meta content="width=device-width, initial-scale=1.0" name="viewport"/>

					<link href="//www.unomaha.edu/_files/css/default-001-header-footer.css" rel="stylesheet"/>
					<script src="//www.unomaha.edu/_files/js/modernizr-2.5.3.min.js"></script>
					<script src="//www.unomaha.edu/_files/js/respond.min.js"></script>
					<link href="//www.unomaha.edu/_files/css/colorbox/colorbox.css" media="screen" rel="stylesheet" type="text/css"/>
					<link rel="stylesheet" href="//www.ist.unomaha.edu/css/template_fixes.css">
				</head>

				<body>
				<div class="subsite" id="content">
					<div class="visible-mobile" id="header_mobile">
						<div class="main-header clearfix">
							<div class="inner-content">
								<div class="subsite-logos">
									<div>
										<a class="home-logo" href="http://www.unomaha.edu/">
											<img alt="University of Nebraska Omaha" src="https://www.unomaha.edu/_files/images/logo-subsite-o.png"/>
										</a>
									</div>
									<div>
										<!-- USER HEADER MOBILE -->
										<a class="college" href="http://unomaha.edu">University of Nebraska Omaha</a>
										<a class="department" href="http://www.unomaha.edu/college-of-information-science-and-technology/">College of Information Science &amp; Technology</a>
										<!-- /USER HEADER MOBILE -->
									</div>
								</div>
							</div>
						</div>
					</div>

					<nav></nav>
					<div class="hide-mobile" id="header">
						<div class="main-header clearfix">
							<div class="inner-content">
								<div class="subsite-logos">

									<div class="home-logo">
										<a href="http://www.unomaha.edu/">
											<img alt="University of Nebraska Omaha" src="https://www.unomaha.edu/_files/images/logo-subsite-o-2.png"/>
										</a>
									</div>
									<div>
										<!-- USER HEADER DESKTOP -->
										<a class="college" href="http://unomaha.edu">University of Nebraska Omaha</a>
										<a class="department" href="http://www.unomaha.edu/college-of-information-science-and-technology/">College of Information Science &amp; Technology</a>
										<!-- /USER HEADER DESKTOP -->
									</div>
									<div id="sup-navigation">
										<div class="search">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="nav" class="navbar">
							<div class="inner-content"><ul class="nav clearfix">
									<li class="dropdown">
										<a href="" class="dropdown-toggle" data-toggle="dropdown">Dropdown Menu Item <b class="caret"> </b></a>
										<ul class="dropdown-menu">
											<li><a href="">One</a></li>
											<li><a href="">Two</a></li>
										</ul>
									</li>
									<li><a href="">Menu Item Without Dropdown</a></li>
								</ul></div>
						</div>
					</div>
					<div role="main">
						<div id="hero">
							<div class="inner-content">
								<h1>Page Header</h1>
							</div>
						</div>
						<div id="breadcrumbs">
							<div class="inner-content">
								<div class="row-fluid">
									<div class="span12">
										<ul class="breadcrumb">
											<li><a href="">UNO</a></li>
											<li><a href="">College of Information Science &amp; Technology</a></li>
											<li><a href="">This Page</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-2 nav-r" id="content_main">
							<div class="inner-content">
								<!-- USER MAIN CONTENT -->
								<h2>First-level content heading</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquam faucibus scelerisque. Aliquam in purus porttitor, rhoncus dui eu, mattis ante. Aliquam vel mi risus. Ut et ligula vitae lorem tempus finibus. Fusce maximus ac risus quis volutpat. Morbi a elit velit. <strong>Phasellus risus est, congue sed cursus id, mollis sit amet est.</strong> In vulputate est ut mi semper tempus. Duis ac ipsum iaculis, venenatis lectus sit amet, rutrum est. Proin purus erat, fermentum eu hendrerit quis, lacinia in ante. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam at lorem sapien. Curabitur non magna id augue malesuada aliquam. <i>Quisque consectetur placerat velit.</i> Fusce accumsan ac lectus quis varius. In cursus, velit et facilisis gravida, quam velit condimentum quam, tincidunt rutrum tortor lacus nec augue. </p>

								<h3>Second-level content heading</h3>

								<ul class="standard">
									<li>Lorem</li>
									<li>Ipsom</li>
									<li>Dolor</li>
								</ul>

								<h4>Third-level content heading</h4>
								<table class="gridder">
									<thead>
									<tr>
										<th>What a</th>
										<th>Pretty</th>
										<th>Table</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>Lorem ipsum dolor</td>
										<td>adipiscing elit. Morbi</td>
										<td>Integer imperdiet ullamcorper</td>
									</tr>
									<tr>
										<td>sit amet, consectetur</td>
										<td>nec efficitur urna.</td>
										<td>mollis. Mauris quis.</td>
									</tr>
									<tr>
										<td>Proin purus erat,</td>
										<td>consectetur placerat velit.</td>
										<td>tincidunt rutrum tortor</td>
									</tr>
									</tbody>
								</table>

								<a href="" class="btn-large btn-cta-red">Submit</a>
								<a href="" class="btn btn-cta-red">View Details</a>
								<a href="" class="btn">Cancel</a>

								<h5>Grid layouts are also possible -- just change the number in the class</h5>
								<div class="row-fluid">
									<div class="span8">
										<p>Vivamus quis risus quis tellus dignissim lacinia vitae non eros. Nulla sollicitudin elit vitae nulla porttitor mollis. Sed vitae semper nulla. Nam eget mauris luctus, porta eros eu, maximus velit. Nunc convallis lectus urna. Vivamus at porttitor nulla. Etiam tempor malesuada urna vitae molestie. Quisque finibus nec quam vitae cursus. Aliquam eget magna ante. Donec et hendrerit est. Donec ornare leo a posuere iaculis. Morbi nunc augue, fermentum at libero id, vestibulum condimentum justo. In sagittis varius est, non pulvinar nunc tincidunt eu. Proin sed porta leo, non dapibus diam. Mauris aliquam velit tempor metus dignissim scelerisque. </p>
									</div>
									<div class="span4">
										<p>Cras auctor tincidunt venenatis. Phasellus ac nibh et erat rhoncus consequat a id massa. Quisque a lectus vel libero vehicula elementum ut ac dui. Praesent id ullamcorper eros. Nunc ligula quam, tristique vitae quam sit amet, sollicitudin efficitur nisl. Maecenas mattis cursus erat in porttitor. Curabitur dapibus velit eget accumsan venenatis. Vivamus non tempor dolor. </p>
									</div>
								</div>

								<!-- /USER MAIN CONTENT -->
							</div>
						</div>
					</div>
					<footer>
						<div class="inner-content">
							<div class="mobile-clearfix">
								<div class="span5 alpha">
									<ul>
										<li class="column-header">NEXT STEPS</li>
										<li><a href="http://www.unomaha.edu/admissions/">Apply for Admission</a></li>
										<li><a href="http://www.unomaha.edu/admissions/visit/">Visit the Campus</a></li>
										<li><a href="http://www.unomaha.edu/tour/">Take a Virtual Tour</a></li>
										<li><a href="https://ebruno.unomaha.edu/crm/rfi/">Request Information</a></li>
									</ul>
								</div>
								<div class="span5">
									<ul>
										<li class="column-header">JUST FOR YOU</li>
										<li><a href="http://www.unomaha.edu/admissions/">Future Students</a></li>
										<li><a href="http://www.unomaha.edu/current.php">Current Students</a></li>
										<li><a href="http://www.unomaha.edu/humanresources/employment.php">Work at UNO</a></li>
										<li><a href="http://www.unomaha.edu/facstaff.php">Faculty &amp; Staff</a></li>
									</ul>
								</div>
							</div>
							<div class="mobile-clearfix">
								<div class="span5">
									<ul>
										<li class="column-header">RESOURCES</li>
										<li><a href="http://my.unomaha.edu/">my.unomaha.edu</a></li>
										<li><a href="http://events.unomaha.edu/">Calendars</a></li>
										<li><a href="http://www.unomaha.edu/maps/">Campus Map</a></li>
										<li><a href="http://library.unomaha.edu/">Library</a></li>
										<li><a href="http://registrar.unomaha.edu/catalogs.php">Course Catalog</a></li>
										<li><a href="http://cashiering.unomaha.edu/">Pay Your Bill</a></li>
									</ul>
								</div>
								<div class="span5">
									<ul>
										<li class="column-header">AFFILIATES</li>
										<li><a href="http://www.nebraska.edu/">Nebraska System</a></li>
										<li><a href="http://pki.nebraska.edu/">Peter Kiewit Institute</a></li>
										<li><a href="http://nufoundation.org/Page.aspx?pid=375">Campaign for Nebraska</a></li>
									</ul>
								</div>
							</div>
							<div class="mobile-clearfix">
								<div class="span5 omega">
									<ul class="social">
										<li class="column-header">CONNECT</li>
										<li><a class="phone" href="tel:402.554.2380">&#160;402.554.2380</a></li>
										<li><a class="facebook" href="https://www.facebook.com/unocist" target="_blank">&#160;Facebook</a></li>
										<li><a class="twitter" href="https://twitter.com/unocist" target="_blank">&#160;Twitter</a></li>
										<li><a class="youtube" href="https://www.youtube.com/channel/UClrgDqDeLKtshxX69HcOGlQ" target="_blank">&#160;YouTube</a></li>
										<li><a class="instagram" href="http://instagram.com/unocist/" target="_blank">&#160;Instagram</a></li>
										<li><a class="enotes" href="https://www.unomaha.edu/news/maverick-daily/" target="_blank">&#160;The Maverick Daily</a></li>
									</ul>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span9"><a href="/" id="footer-logo">University of Nebraska Omaha</a>
									<p>University of Nebraska Omaha, 6001 Dodge Street, Omaha, NE 68182</p>
									<p>&#169; 2014 | <a href="http://emergency.unomaha.edu/">Emergency Information</a> <span class="footer-alert">Alert</span></p>
								</div>
							</div>
						</div>
					</footer>

				</div>

				<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
				<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
				<script src="//www.unomaha.edu/_files/js/respond.min.js"></script>
				<script src="//www.unomaha.edu/_files/js/jquery.flexslider-min.js" type="text/javascript"></script>
				<script src="//www.unomaha.edu/_files/js/bootstrap.min.js"></script>
				<script src="//www.unomaha.edu/_files/js/bootstrap-hover-dropdown.min.js"></script>
				<script src="//www.unomaha.edu/_files/js/jquery.foundation.navigation.js"></script>
				<script src="//www.unomaha.edu/_files/js/jquery.mousewheel.min.js"></script>
				<script src="//www.unomaha.edu/_files/js/jquery.mCustomScrollbar.min.js"></script>
				<script src="//www.unomaha.edu/_files/js/jquery-picture-min.js"></script>
				<script src="//www.unomaha.edu/_files/js/script.js"></script>

				<script>
					var _gaq=[['_setAccount','UA-2777175-1'],['_trackPageview']];
					(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
						g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
						s.parentNode.insertBefore(g,s)}(document,'script'));
				</script>


				<!-- USER SCRIPTS -->

				<!-- /USER SCRIPTS -->

				</body>
				</html>
			</div>
		</div>
	</body>
</html>
