<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@login');
Route::get('Login', 'WelcomeController@login');
Route::get('app', 'WelcomeController@app');
Route::get('Theme', 'WelcomeController@theme');
Route::get('/auth.login', 'WelcomeController@login');
route::get('home', 'PagesController@home');
route::post('welcome','Auth\AuthController@authenticate');

route::get('createProfile','PagesController@studentProfile');

route::post('Profile','PagesController@CreateProfile');
route::get('Plan','PagesController@CreatePlan');
route::get('Delete','PagesController@DeleteProfile');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
