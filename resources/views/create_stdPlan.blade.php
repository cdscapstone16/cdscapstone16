<!Ds
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"><!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>University of Nebraska Omaha | <!-- Page Title --></title>
    <meta content="" name="description"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

    <link href="//www.unomaha.edu/_files/css/default-001-header-footer.css" rel="stylesheet"/>
    <script src="//www.unomaha.edu/_files/js/modernizr-2.5.3.min.js"></script>
    <script src="//www.unomaha.edu/_files/js/respond.min.js"></script>
    <script>
        function inputFocus(i){
            if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
        }
        function inputBlur(i){
            if(i.value==""){ i.value=i.defaultValue; i.style.color="#888"; }
        }
    </script>
    <script>
        function loadmessage() {
            alert('Are you sure you want to Cancel Editing the plan')
        }
    </script>
    <style>
        .right {
            text-align: right;
            float: right;
        }
    </style>

    <link href="//www.unomaha.edu/_files/css/colorbox/colorbox.css" media="screen" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//www.ist.unomaha.edu/css/template_fixes.css">
</head>

<body>
<div class="subsite" id="content">
    <div class="visible-mobile" id="header_mobile">
        <div class="main-header clearfix">
            <div class="inner-content">
                <div class="subsite-logos">
                    <div>
                        <a class="home-logo" href="http://www.unomaha.edu/">
                            <img alt="University of Nebraska Omaha" src="https://www.unomaha.edu/_files/images/logo-subsite-o.png"/>
                        </a>
                    </div>
                    <div>
                        <!-- USER HEADER MOBILE -->
                        <a class="college" href="http://unomaha.edu">University of Nebraska Omaha</a>
                        <a class="department" href="http://www.unomaha.edu/college-of-information-science-and-technology/">College of Information Science &amp; Technology</a>
                        <!-- /USER HEADER MOBILE -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav></nav>
    <div class="hide-mobile" id="header">
        <div class="main-header clearfix">
            <div class="inner-content">
                <div class="subsite-logos">

                    <div class="home-logo">
                        <a href="http://www.unomaha.edu/">
                            <img alt="University of Nebraska Omaha" src="https://www.unomaha.edu/_files/images/logo-subsite-o-2.png"/>
                        </a>
                    </div>
                    <div>
                        <!-- USER HEADER DESKTOP -->
                        <a class="college" href="http://unomaha.edu">University of Nebraska Omaha</a>
                        <a class="department" href="http://www.unomaha.edu/college-of-information-science-and-technology/">College of Information Science &amp; Technology</a>
                        <!-- /USER HEADER DESKTOP -->
                    </div>
                    <div id="sup-navigation">
                        <div class="search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div role="main">
        <div id="hero">
            <div class="inner-content">
                <h1>Course Demand Projection System</h1>
            </div>
        </div>
        <div id="breadcrumbs">
            <div class="inner-content">
                <div class="row-fluid">
                    <div class="span12">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-2 nav-r" id="content_main">
            <div class="inner-content">
                <!-- USER MAIN CONTENT -->
                <h2>Student Plan</h2>
                <br>
                <br>


                <!-- id="firstname" placeholder="First Name:" -->
                <br>
                <br>
                <div class="right">
                    <a href="" class="btn-large btn-cta-red"  >Create Plan</a>
                    <a href="" class="btn-large btn-cta-red" onclick='loadmessage()' >Cancel</a>
                </div>


                <!-- /USER MAIN CONTENT -->
            </div>
        </div>
    </div>
    <footer>
        <div class="inner-content">
            <div class="mobile-clearfix">
                <div class="span5 alpha">
                    <ul>
                        <li class="column-header">NEXT STEPS</li>
                        <li><a href="http://www.unomaha.edu/admissions/">Apply for Admission</a></li>
                        <li><a href="http://www.unomaha.edu/admissions/visit/">Visit the Campus</a></li>
                        <li><a href="http://www.unomaha.edu/tour/">Take a Virtual Tour</a></li>
                        <li><a href="https://ebruno.unomaha.edu/crm/rfi/">Request Information</a></li>
                    </ul>
                </div>
                <div class="span5">
                    <ul>
                        <li class="column-header">JUST FOR YOU</li>
                        <li><a href="http://www.unomaha.edu/admissions/">Future Students</a></li>
                        <li><a href="http://www.unomaha.edu/current.php">Current Students</a></li>
                        <li><a href="http://www.unomaha.edu/humanresources/employment.php">Work at UNO</a></li>
                        <li><a href="http://www.unomaha.edu/facstaff.php">Faculty &amp; Staff</a></li>
                    </ul>
                </div>
            </div>
            <div class="mobile-clearfix">
                <div class="span5">
                    <ul>
                        <li class="column-header">RESOURCES</li>
                        <li><a href="http://my.unomaha.edu/">my.unomaha.edu</a></li>
                        <li><a href="http://events.unomaha.edu/">Calendars</a></li>
                        <li><a href="http://www.unomaha.edu/maps/">Campus Map</a></li>
                        <li><a href="http://library.unomaha.edu/">Library</a></li>
                        <li><a href="http://registrar.unomaha.edu/catalogs.php">Course Catalog</a></li>
                        <li><a href="http://cashiering.unomaha.edu/">Pay Your Bill</a></li>
                    </ul>
                </div>
                <div class="span5">
                    <ul>
                        <li class="column-header">AFFILIATES</li>
                        <li><a href="http://www.nebraska.edu/">Nebraska System</a></li>
                        <li><a href="http://pki.nebraska.edu/">Peter Kiewit Institute</a></li>
                        <li><a href="http://nufoundation.org/Page.aspx?pid=375">Campaign for Nebraska</a></li>
                    </ul>
                </div>
            </div>
            <div class="mobile-clearfix">
                <div class="span5 omega">
                    <ul class="social">
                        <li class="column-header">CONNECT</li>
                        <li><a class="phone" href="tel:402.554.2380">&#160;402.554.2380</a></li>
                        <li><a class="facebook" href="https://www.facebook.com/unocist" target="_blank">&#160;Facebook</a></li>
                        <li><a class="twitter" href="https://twitter.com/unocist" target="_blank">&#160;Twitter</a></li>
                        <li><a class="youtube" href="https://www.youtube.com/channel/UClrgDqDeLKtshxX69HcOGlQ" target="_blank">&#160;YouTube</a></li>
                        <li><a class="instagram" href="http://instagram.com/unocist/" target="_blank">&#160;Instagram</a></li>
                        <li><a class="enotes" href="https://www.unomaha.edu/news/maverick-daily/" target="_blank">&#160;The Maverick Daily</a></li>
                    </ul>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span9"><a href="/" id="footer-logo">University of Nebraska Omaha</a>
                    <p>University of Nebraska Omaha, 6001 Dodge Street, Omaha, NE 68182</p>
                    <p>&#169; 2014 | <a href="http://emergency.unomaha.edu/">Emergency Information</a> <span class="footer-alert">Alert</span></p>
                </div>
            </div>
        </div>
    </footer>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
<script src="//www.unomaha.edu/_files/js/respond.min.js"></script>
<script src="//www.unomaha.edu/_files/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="//www.unomaha.edu/_files/js/bootstrap.min.js"></script>
<script src="//www.unomaha.edu/_files/js/bootstrap-hover-dropdown.min.js"></script>
<script src="//www.unomaha.edu/_files/js/jquery.foundation.navigation.js"></script>
<script src="//www.unomaha.edu/_files/js/jquery.mousewheel.min.js"></script>
<script src="//www.unomaha.edu/_files/js/jquery.mCustomScrollbar.min.js"></script>
<script src="//www.unomaha.edu/_files/js/jquery-picture-min.js"></script>
<script src="//www.unomaha.edu/_files/js/script.js"></script>

<script>
    var _gaq=[['_setAccount','UA-2777175-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g,s)}(document,'script'));
</script>


<!-- USER SCRIPTS -->

<!-- /USER SCRIPTS -->

</body>
</html>